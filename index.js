
// Exponent Operator

// First method
/*
const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);
*/

// Second method
let exponent = (num) => {
	let expo = num ** 3
	return `The cube of ${num} is ${expo}`;
}

const getCube = exponent(2);
console.log(getCube);



// Array Deconstructing
const address = ['258 Washington Ave NW', 'California 90011'];

const [streetNumber, stateAndCode] = address

console.log(`I live at ${streetNumber}, ${stateAndCode}`);


// Object Deconstructing
const animal = {
	name: 'Lolong',
	kind: 'saltwater crocodile',
	weight: '1075 kgs',
	size: '20 ft 3 in'
};

const {name, kind, weight, size} = animal;

console.log(`${name} was a ${kind}. He weighed at ${weight} with a measurement of ${size}.`);

// Array Loop

const arrayOfNumbers = [1, 2, 3, 4, 5];

arrayOfNumbers.forEach((arrayOfNumber) => {
	console.log(arrayOfNumber);
});



const reduceNumber = arrayOfNumbers.reduce((arrayOfNumber, curValue) => {
	return arrayOfNumber + curValue;
});
console.log(reduceNumber);



// Class

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

const myDog = new Dog();

myDog.name = 'Frankie',
myDog.age = 5,
myDog.breed = 'Miniature Dachshund'

console.log(myDog); 












